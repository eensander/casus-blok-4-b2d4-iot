<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
    'register' => false, // Registration Routes...
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
]);

Route::view('/', 'pages.front.welcome')->name('front.welcome');
Route::view('/about', 'pages.front.about')->name('front.about');

Route::get('/home', 'HomeController@index')->name('back.home');

Route::get('/dashboard', 'DashboardController@index')
    ->name('back.dashboard')
    ->middleware('auth');


Route::prefix('/api')->middleware('auth')->group(function () {
    Route::get('/floors', 'ApiController@floors');
    Route::get('/update_device/{device_id}/{status}', 'ApiController@update_device');
    Route::get('/user', 'ApiController@user');
});
