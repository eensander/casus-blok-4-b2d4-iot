@extends('layouts.boilerplate')

@section('hardcoded_styles')

@endsection

@section('body')
        <div class="flex flex-col justify-center items-center relative h-screen">


            <div class="mb-4 text-content">
                <div>
                    <h1 class="text-6xl font-thin">About</h1>
                </div>
            </div>

            <div class="links mb-2">
                <a href="{{ route('login') }}">Login</a>
                <a href="{{ route('welcome') }}">Home</a>
            </div>

            <div class="max-w-screen-md">
                <h2 class="text-3xl font-normal">Lorem ipsum</h2>
                Dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Eu lobortis elementum nibh tellus molestie nunc non blandit massa. Mauris augue neque gravida in fermentum et. Euismod nisi porta lorem mollis aliquam. Sit amet cursus sit amet dictum sit amet justo. Gravida in fermentum et sollicitudin ac. Fames ac turpis egestas maecenas pharetra convallis posuere morbi. Duis convallis convallis tellus id interdum velit laoreet id. Amet facilisis magna etiam tempor orci. Pharetra diam sit amet nisl suscipit adipiscing. Sed adipiscing diam donec adipiscing. Integer vitae justo eget magna fermentum iaculis.
            </div>


        </div>

@endsection
