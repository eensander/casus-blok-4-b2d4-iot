@extends('layouts.boilerplate')

@section('hardcoded_styles')
@endsection

@section('body')
        <div class="flex flex-col justify-center items-center relative h-screen">


            <div class="mb-4 text-content">
                <div>
                    <span class="text-7xl font-thin">IOT Applications</span>
                </div>
            </div>

            <div class="links">
                <a href="{{ route('login') }}">Login</a>
                <a href="{{ route('front.about') }}">About</a>
            </div>

        </div>

@endsection
