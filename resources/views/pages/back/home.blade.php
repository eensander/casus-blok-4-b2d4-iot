@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Home</div>

                <div class="card-body">
                    Ga naar het <a class="btn-link" href="{{ route('back.dashboard') }}">Dashboard</a> om de apparaten in uw kamer te beheren
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
