@extends('layouts.app')

@section('hardcoded_scripts')
<script defer>
    // alert('gang');

    // $('g').each(function(e){
    //     alert('g');
    // });

    $(document).ready(function() {

        var current_floor_id = {{ $current_floor }};
        var current_floor_element = $('.floorplan[data-floor-id="'+current_floor_id+'"]');

        function reloadFloor()
        {
            current_floor_element = $('.floorplan[data-floor-id="'+current_floor_id+'"]');

            $('.floorplan').addClass('hidden');
            current_floor_element.removeClass('hidden');
            $('#floor-buttons #floor-current-level').text(current_floor_element.data('floor-level'));
        }
        reloadFloor();

        $('#floor-buttons #floor-previous').on('click', function() {
            if ($('.floorplan[data-floor-id="'+(current_floor_id-1)+'"]').length !== 0) {
                current_floor_id--;
                reloadFloor();
            }
        });

        $('#floor-buttons #floor-next').on('click', function() {
            if ($('.floorplan[data-floor-id="'+(current_floor_id+1)+'"]').length !== 0) {
                current_floor_id++;
                reloadFloor();
            }
        });

    });

</script>
@endsection

@section('hardcoded_styles')
<style>
    [data-room] rect {
        fill: red;
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">

                    {{-- <div id="floorplans">

                        @foreach($floorplans as $floorplan)

                            <div id="floor-{{ $floorplan->id }}" class="floorplan hidden" data-floor-id="{{ $floorplan->id }}" data-floor-level="{{ $floorplan->floor_level }}">

                                <object class="w-full" data="{{ asset('assets/floorplans/' . $floorplan->floor_image_filename) }}" type="image/svg+xml"></object>

                                @foreach($floorplan->rooms as $room)
                                    <h2>{{ $room->room_number }}</h2>
                                @endforeach

                            </div>
                        @endforeach

                    </div>

                    <div id="floor-buttons" class="w-full flex justify-center">

                        <button class="text-5xl px-4 text-blue-600 hover:text-blue-800" id="floor-previous">&lt;</button>
                        <span class="text-4xl px-4 my-auto" id="floor-current-level"></span>
                        <button class="text-5xl px-4 text-blue-600 hover:text-blue-800" class="" id="floor-next">&gt;</button>

                    </div> --}}

                    <floorplan-controller></floorplan-controller>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
