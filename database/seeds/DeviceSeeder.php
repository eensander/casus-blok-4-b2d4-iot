<?php

use Illuminate\Database\Seeder;

use App\Floor;
use App\Rooms;
use App\Device;
use App\DeviceType;

class DeviceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // floors
        DB::table('floors')->insert([
            [
                'floor_image_filename' => 'floorplan-v2-level_-1.svg',
                'floor_level' => -1,
            ],[
                'floor_image_filename' => 'floorplan-v4-level_0.svg',
                'floor_level' => 0,
            ],[
                'floor_image_filename' => 'floorplan-v4-level_1.svg',
                'floor_level' => 1,
            ]
        ]);

        // device types
        DB::table('device_types')->insert([
            [
                'device_type_name' => 'lamp',
                'device_type_icon_filename' => 'lightbulb-solid.svg',
            ],[
                'device_type_name' => 'tv',
                'device_type_icon_filename' => 'tv-solid.svg',
            ],[
                'device_type_name' => 'gordijn',
                'device_type_icon_filename' => 'booth-curtain.svg',
            ],
        ]);

        $room_data = [
            1 => [                          // floor-level => rooms
                '1.1' => [                  //   room-number => devices
                    'points' => '7,7 207,7 207,367 7,367',
                    'devices' => [
                        5 => 'tv',            //     domoticz_id => device-type
                        9 => 'gordijn',
                        1 => 'lamp',
                    ]
                ],
                '1.2' => [
                    'points' => '837,7 1047,7 1047,247 837,247',
                    'devices' => [
                        6 => 'tv',
                        10 => 'gordijn',
                        2 => 'lamp',
                    ]
                ],
                '1.3' => [
                    'points' => '1057,7 1267,7 1267,247 1057,247',
                    'devices' => [
                        7 => 'tv',
                        11 => 'gordijn',
                        3 => 'lamp',
                    ]
                ],
                '1.4' => [
                    'points' => '1277,7 1487,7 1487,247 1277,247',
                    'devices' => [
                        8 => 'tv',
                        12 => 'gordijn',
                        4 => 'lamp',
                    ]
                ],
            ],
        ];

        // rooms
        foreach($room_data as $floor_level => $room_array)
        {
            foreach($room_array as $room_number => $room)
            {
                // rooms
                $room_id = DB::table('rooms')->insertGetId([
                    'floor_id' => Floor::where('floor_level', $floor_level)->first()->id,
                    'room_number' => $room_number,
                    'svg_points' => $room['points'],
                ]);

                // devices
                foreach($room['devices'] as $domoticz_id => $device_type)
                {

                    DB::table('devices')->insert([
                        'domoticz_id' => $domoticz_id,
                        'room_id' => $room_id,
                        'device_type_id' => DeviceType::where('device_type_name', $device_type)->first()->id,
                    ]);
                }
            }
        }

    }

}
