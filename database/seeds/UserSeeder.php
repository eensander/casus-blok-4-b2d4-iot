<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\Room;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'role_name' => 'patient'
            ],[
                'role_name' => 'observer'
            ],[
                'role_name' => 'admin'
            ]
        ]);

        // admin
        DB::table('users')->insert([
            'username' => 'admin',
            'role_id' => Role::where('role_name', 'admin')->first()->id,
            'password' => Hash::make('password'),
        ]);

        // observer
        DB::table('users')->insert([
            'username' => 'observer',
            'role_id' => Role::where('role_name', 'observer')->first()->id,
            'password' => Hash::make('password'),
        ]);

        // rooms
        DB::table('users')->insert([
            'username' => 'patient-room-1.1',
            'role_id' => Role::where('role_name', 'patient')->first()->id,
            'room_id' => Room::where('room_number', '1.1')->first()->id,
            'password' => Hash::make('password'),
        ]);
        DB::table('users')->insert([
            'username' => 'patient-room-1.2',
            'role_id' => Role::where('role_name', 'patient')->first()->id,
            'room_id' => Room::where('room_number', '1.2')->first()->id,
            'password' => Hash::make('password'),
        ]);
        DB::table('users')->insert([
            'username' => 'patient-room-1.3',
            'role_id' => Role::where('role_name', 'patient')->first()->id,
            'room_id' => Room::where('room_number', '1.3')->first()->id,
            'password' => Hash::make('password'),
        ]);
        DB::table('users')->insert([
            'username' => 'patient-room-1.4',
            'role_id' => Role::where('role_name', 'patient')->first()->id,
            'room_id' => Room::where('room_number', '1.4')->first()->id,
            'password' => Hash::make('password'),
        ]);
    }
}
