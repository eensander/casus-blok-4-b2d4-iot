
# Casus Blok 4 - B2D4 - IOT Applications

# Installeren

1. Clone de repository
```
$ git clone git@bitbucket.org:eensander/casus-blok-4-b2d4-iot.git
```

2. Installeer dependencies
```
$ composer install
$ npm install
```

3. Maak de omgeving klaar en vul databasegegevens in
```
$ cp .env.example .env
```

4. Vul laatste commandos in
```
$ php artisan key:generate
$ php artisan migrate:fresh --seed
$ npm run dev
```

5. Draai de server
```
$ php artisan serve
```

6. Start de domoticz server op en restore de configuratie

## Inloggegevens

| gebruikersnaam  | wachtwoord  | kamer
|--|--|--|
| admin | password | *alle kamers*
| observer | password | *alle kamers*
| patient-room-1.1 | password | 1.1
| patient-room-1.2 | password | 1.2
| patient-room-1.3 | password | 1.3
| patient-room-1.4 | password | 1.4
