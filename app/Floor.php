<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Floor extends Model
{

    public function rooms()
    {
        return $this->hasMany('App\Room', 'floor_id', 'id');
    }

}
