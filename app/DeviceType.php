<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeviceType extends Model
{

    public function device() {
        return $this->hasMany('App\Device', 'device_type_id', 'id');
    }

}
