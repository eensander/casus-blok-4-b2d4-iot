<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{

    public function device_type() {
        return $this->hasOne('App\DeviceType', 'id', 'device_type_id');
    }

}
