<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{

    public function devices()
    {
        return $this->hasMany('App\Device', 'room_id', 'id');
    }

    public function user()
    {
        return $this->belongsto('App\User', 'room_id');
    }

}
