<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Floor;

class DashboardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        // if (in_array(Auth::user()->role->role_name, ['observer', 'admin']))
        // {
            // $floorplans =
        // }

        // dd(Floor::where('floor_level', 1)->first()->rooms());

        return view('pages.back.dashboard', [
            'floorplans' => Floor::all(),
            'current_floor' => Floor::where('floor_level', 1)->first()->id,
        ]);
    }
}
