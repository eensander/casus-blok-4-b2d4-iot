<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Floor;


class ApiController extends Controller
{


    public function user()
    {
        $user = Auth::user();

        return [
            'user_id' => $user->user_id,
            'username' => $user->username,
            'role' => $user->role->role_name,
            'room_id' => $user->room_id,
        ];
    }

    public function update_device($device_id, $status)
    {
        $device = \App\Device::where('domoticz_id', $device_id)->first();

        if ($device != null)
        {
            if ($status == 'On')
                $device->device_status = true;
            else
                $device->device_status = false;
            $device->save();

            return 'true';
        }
        else
        {
            return 'false';
        }
    }

    public function floors()
    {
        $floors = [];
        foreach(Floor::all() as $floor) {

            $rooms = [];
            foreach($floor->rooms as $room) {

                $devices = [];
                foreach($room->devices as $device)
                {
                    $devices[] = [
                        'device_id' => $device->id,
                        'room_id' => $device->room_id,
                        'floor_id' => $floor->id,
                        'device_type_id' => $device->device_type_id,
                        'device_type' => $device->device_type->device_type_name,
                        'device_type_icon_filename' => $device->device_type->device_type_icon_filename,
                        'device_status' => $device->device_status,
                        'domoticz_id' => $device->domoticz_id,
                    ];
                }

                $rooms[] = [
                    'room_id' => $room->id,
                    'floor_id' => $room->floor_id,
                    'room_number' => $room->room_number,
                    'svg_points' => $room->svg_points,
                    'devices' => $devices
                ];
            }

            $floors[] = [
                'floor_id' => $floor->id,
                'floor_level' => $floor->floor_level,
                'floor_image_filename' => $floor->floor_image_filename,
                'rooms' => $rooms
            ];
        }
        return [
            'floors' => $floors,
            'current_floor_level' => 1,
            'level_max' => Floor::max('floor_level'),
            'level_min' => Floor::min('floor_level'),
        ];
    }

}
